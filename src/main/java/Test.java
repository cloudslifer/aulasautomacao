
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Test {

    private WebDriver driver;

    @Before
    public void criarDriver(){
        Driver wdriver = new Driver();
        driver = wdriver.Webdriver();
    }


    @org.junit.Test
    public void acessarDuvidas(){
        driver.findElement(By.id("Duvidas")).click();
    }


    @org.junit.Test
    public void fazerBusca(){
        String termoBusca = "panela";


        driver.findElement(By.id("strBusca")).sendKeys(termoBusca.toLowerCase());
        driver.findElement(By.id("btnOK")).click();

        List<WebElement> resultados = driver.findElements(By.cssSelector(".nm-product-name>a>span"));

        System.out.println("Quantidade de itens encontrados: " + resultados.size());
        System.out.println("Itens encontrados: ");


        for (int i = 0; i < resultados.size(); i++) {
            System.out.println(i + ": "+ resultados.get(i).getText());
        }

    }

    @After
    public void fecharDriver(){
        driver.close();
        driver.quit();
    }


}
